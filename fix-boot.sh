#!/usr/bin/env sh

lsblk
read "Write the device where you want to install the bootloader" device
grub-install --recheck --no-floppy "$device"
grub-mkconfig -o /boot/grub2/grub.cfg
mkinitrd
