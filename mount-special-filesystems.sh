#!/usr/bin/env sh

# @param $1 mount directory

mount -t proc none "$1/proc"
mount -o bind /dev "$1/dev"
mount -t sysfs sys "$1/sys"
mount -t efivarfs efivarfs "$1/firmware/efi/efivars"
