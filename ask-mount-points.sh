#!/usr/bin/env sh

read "Where do you want to chroot?" chrootDir

lsblk
blkid

echo "\n"

read -p "Write where / is located" partition
mount "$partition" "$chrootDir"

read -p "Write where /boot is located" partition
mount "$partition" "$chrootDir"/boot

read -p "Write where /boot/efi is located" partition
mount "$partition" "$chrootDir"/boot/efi

read -p "Write where /home is located" partition
mount "$partition" "$chrootDir"/home
